import sys


def main(name="John"):
    '''
    Main function

    Parameters
    -----------
        name: str, default "John"
            The name to print
    '''
    print(f"Hello {name}")


if __name__ == "__main__":
    main("Read the Docs")
